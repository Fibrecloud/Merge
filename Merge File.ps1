# Error Variable Refresh
$Error.Clear()

# Master Variable Refresh
$PowerShell = [PowerShell]::Create()
$PowerShell.AddScript('Get-Variable | Select-Object -ExpandProperty "Name"') | Out-Null
$VariableList = $PowerShell.Invoke()
$PowerShell.Dispose()
Remove-Variable (Get-Variable | Select-Object -ExpandProperty "Name" | Where-Object { $VariableList -NotContains $_ }) -ErrorAction "SilentlyContinue"

# Console Title
$Host.UI.RawUI.WindowTitle = "Windows PowerShell: Merge File"

# Define Error Verbose
$ErrorActionPreference = "SilentlyContinue"

# Character Display Correction
$OutputEncoding = [Console]::InputEncoding = [Console]::OutputEncoding = New-Object System.Text.UTF8Encoding

# Console Screen Refresh
[System.Console]::Clear()

# Console Error Information
$ShowError = $False

while (!($ExitMode -EQ $True)) {

    while ((!($RunningMode -EQ $True)) -AND (!($ExitMode -EQ $True))) {

        if ([String]::IsNullOrEmpty($InitialExecution)) {

            # Define Variable
            $InitialExecution = $True

            # System Tray Message Maximum Length
            # $TrayIcon.Text = "———————————————————" + "`n" + "• ———————————————————" + "`n" + "• ———————————————————"

            # System Tray Icon Indicator
            if ([String]::IsNullOrEmpty($TrayIcon)) {
                [System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null
                [System.Reflection.Assembly]::LoadWithPartialName("System.Drawing") | Out-Null
                $TrayIcon = New-Object System.Windows.Forms.NotifyIcon
                $TrayIcon.Icon = [System.Convert]::FromBase64String("AAABAAIAEBAAAAEAIABoBAAAJgAAACAgAAABACAAqBAAAI4EAAAoAAAAEAAAACAAAAABACAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAANt8SBPefkkc339JHOB/SRzggEkc4IBJHOGBSRzhgUkc4oFJHOOCShzkgksc5IJLHO2HThr8k1QG+ZBTAAAAAADef0ip4YBK5d9/SePdfUfj2nxG49h7RuPVekXj03hE49B2QuPOdEHjzXNB4810QuTTeEXf6oZMWN9/SQD+kVMA339JseWCS//igUr/24tc/86BU//Rd0P/0XdD/89/Uv/JimT/xohk/8SHY/+8d07/wG4+/9t9R5z/sWYE/5JTANt9SIDng0v95odQ//LNuP/q3NL/zJNw/8xzQP/kro//9u3o//Xs5//17un/1ayU/71rPP/UeUXJ/5NVE/WMUADXe0ZM54RL7+mFTP/tpHv/+ubb//Xw7P/UrZT/zohf/96eev/doH3/2p17/8uBV/+/bT7/znZD6e+JTi/mhEsA0ndFIuWDS9vth03/6IRK/+iTYv/1z7r/+/f0/+HLvf/Ihl7/ynI//8tyQP/HcUD/w28//8dyQfjqhkxb4IBJAL1rPg7jgUqz74lO/+yHTf/og0r/5YdS/+61lP/77+j/7+Xf/8yYef/MdUP/y3RC/8dyQf/HcUH/4IBJiE8tFwD/wW0A3X5IiPGJTv/wiU7/7IdN/+eDSv/jgUn/776i///////mxLD/0XlH/892Q//LdEL/x3JB/9h7RrP/mFcO3X5IANV5RVvxiU/484tP/++JTv/rhUv/7aJ3//vt5f/34dX/4ZNn/9Z6Rf/SeET/znZD/8pzQv/VekXb74lOItd7RgDPdkQv7IdN6feNUf/zi0//86R4//zr4v/449j/55dp/95+R//afUf/1npG/9J4RP/OdUP/0XdE7+eES0zIckEAvGs9E+iFTMn7j1H/+JVd//3m2P/87OP/7qF0/+aCSf/igUr/3n9I/9p8R//WekX/0XdE/9F3RP3igUqAvmw+AIdNKwTjgUqc+49S//uVWv/7v53/9aV3/+6HTP/qhkz/5oNL/+KBSv/dfkj/2XxH/9V6Rf/SeET/3X5Isb9tPgDef0gA03hFWOqGTN/wiU7k7YZM4+uFS+PohUzj5oNL4+SCSuPhgUnj339J491+SOPafUfj2HtG5d+ASakAAAAAwW4+ALtrPAbQd0Qa2nxIHNp8RxzafUcc231HHNx+Rxzcfkcc3X5IHN5/SBzef0gc339IHOCASRzlg0sTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP//AAAAAwAAAAMAAAABAAAAAQAAAAEAAAABAAAAAQAAgAAAAIAAAACAAAAAgAAAAIAAAADAAAAAwAAAAP//AAAoAAAAIAAAAEAAAAABACAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADXe0ZQ3H1I/99/Sf/ff0n/339J/99/Sf/ff0n/339J/99/Sf/ef0n/3n9I/95/SP/ef0j/3n9I/95/SP/ef0j/3n9I/95/SP/ef0j/3n9I/95/Sf/ff0n/339J/99/Sf/ff0n/7ohO//6RU2AAAAAAAAAAAAAAAAAAAAAAAAAAANV6RYDjgkr/5IJK/+KBSv/ggEn/3n9I/9x9SP/afEf/13tG/9V6Rf/UeUX/0XdE/892Q//NdUP/y3RC/8lzQf/HckH/xXBA/8NvP//Bbj//v20+/79tPv+/bT7/v20+/79tPv+/bT7/8YpP7wAAAAAAAAAAAAAAAAAAAAAAAAAA1HlFcOOBSv/mg0v/5IJK/+KBSv/ggEn/3n9I/9t9R//ZfEf/13tG/9V6Rf/TeUX/0XdE/892Q//NdUP/y3RC/8lzQf/HckH/xXBA/8NvP//Bbj//v20+/79tPv+/bT7/v20+/79tPv/ff0n//pFTMAAAAAAAAAAAAAAAAAAAAADSeEQw3n9I/+iETP/mg0v/5IJK/+GBSf/ff0n/zpJt/7F0Tf/CcD7/13tG/9V6Rf/TeEX/0XdE/892Q//PhFn/u4dm/7qHZv+6h2b/uYZm/7mGZv+4hmX/sXtY/7FmOP+/bT7/v20+/892Q//+kVNgAAAAAAAAAAAAAAAAAAAAAAAAAADZfEf/6YVM/+eES//lg0v/44JK/+64mf///////////8GfiP+wZjf/13tG/9V5Rf/TeEX/0XdE//z28///////////////////////////////////////qnBL/79tPv+/bT7/w28///6RU58AAAAAAAAAAAAAAAAAAAAAAAAAANJ4Rb/rhk3/6YVM/+eES//lg0v/8cCl/////////////////93Kvf+lZz7/yXNB/9R5Rf/TeEX///////////////////////////////////////////+/e1P/v20+/79tPv+/bT7/8YlP3wAAAAAAAAAAAAAAAAAAAAAAAAAAz3ZDgOmFTP/rhk3/6YVM/+eES//nilb/+ODS//////////////////j08v+xgmL/uGo6/9R5Rf/bkWf/6Lui/+e6of/muqH/5bmg/+S5oP/juKD/05Rw/8JvP//AbT7/v20+/79tPv/mhEv//pFTEAAAAAAAAAAAAAAAAAAAAADNdUNQ4oFK/+2HTf/rhk3/6YVM/+eES//lgkv/8cCl///////////////////////PtaP/pmE0/9B3Q//SeET/0HdE/851Q//MdEL/ynNC/8hyQf/GcUD/xHBA/8FuP/+/bT7/v20+/9d7Rv/+kVNQAAAAAAAAAAAAAAAAAAAAAMx0QhDcfUj/74hO/+2HTf/qhkz/6YVM/+eES//kgkr/6Jls//vv6P/////////////////q39f/qndV/8NwP//SeET/0HdE/851Q//MdEL/ynNC/8dyQf/FcUD/w3A//8FuP/+/bT7/y3RC//6RU4AAAAAAAAAAAAAAAAAAAAAAAAAAANN4Rd/wiU7/7ohO/+yHTf/qhkz/6IVM/+aDS//kgkr/5IlV//bXxv/////////////////49PL/wZ+I/65lN//SeET/0HZE/851Q//LdEL/yXNB/8dyQf/FcUD/w28//8FuP/+/bT7/+I5RvwAAAAAAAAAAAAAAAAAAAAAAAAAAynNCn/CJTv/wiU7/7ohO/+yHTf/qhkz/6IRM/+aDS//kgkr/4oFK/+ywjf/99/T/////////////////3cq9/6RmPv/FcUD/z3ZD/811Q//LdEL/yXNB/8dyQf/FcED/w28//8FuP//th03/AAAAAAAAAAAAAAAAAAAAAAAAAADIckFw6YVM//KKT//wiU7/7ohO/+yHTf/qhkz/6IRM/+aDS//kgkr/4oFK/+aYa//5593/////////////////+PTy/6dvSf/Rd0T/z3ZD/811Q//LdEL/yXNB/8dyQf/FcED/w28//95/SP/7j1IwAAAAAAAAAAAAAAAAAAAAAMdxQTDef0j/9ItQ//KKT//wiU7/7ohO/+yHTf/qhUz/6IRM/+aDS//kgkr/4YFJ/99/Sf/zz7r/////////////////zKyX/9N4Rf/Rd0T/z3ZD/811Q//LdEL/yXNB/8dxQf/EcED/0HdE//qPUnAAAAAAAAAAAAAAAAAAAAAAAAAAANJ4RP/2jFD/9ItQ//KKT//wiU7/7YhN/+uGTf/phUz/54RL/+WDS//jgkr/44hV//nn3f/////////////////Wj2X/1XlF/9N4Rf/Rd0T/z3ZD/811Q//KdEL/yHJB/8ZxQP/IckH/+I5RnwAAAAAAAAAAAAAAAAAAAAAAAAAAyXNBv/iNUf/2jFD/84tP//GKT//viU7/7YdN/+uGTf/phUz/54RL/+WDS//44NL/////////////////6K6M/9l8R//Xe0b/1HlF/9N4Rf/Rd0T/znZD/8x1Qv/Kc0L/yHJB/8ZxQP/sh03fAAAAAAAAAAAAAAAAAAAAAAAAAADDbz+A74lO//eNUf/1jFD/84tP//GKT//viU7/7YdN/+uGTf/phUz/9tG8/////////////////+uvjf/dfkj/2n1H/9h8Rv/Wekb/1HlF/9J4RP/Qd0T/znZD/8x0Qv/Kc0L/yHJB/+SCSv/2jFAQAAAAAAAAAAAAAAAAAAAAAMFuP1Dlg0v/+Y5R//eNUf/1jFD/84tP//GKT//viE7/7YdN//fSvP/////////////////tsI7/4IBJ/95/SP/cfkj/2n1H/9h7Rv/Wekb/1HlF/9J4RP/Qd0T/znVD/8x0Qv/Kc0L/2XxH//SLUFAAAAAAAAAAAAAAAAAAAAAAwG4+ENd7Rv/7j1L/+Y5R//eNUf/1jFD/84tP//GJT//5073/////////////////5ayL/+SCSv/igUr/4IBJ/95/SP/cfkj/2n1H/9h7Rv/Wekb/1HlF/9J4RP/Qd0T/znVD/8x0Qv/PdkP/84tPgAAAAAAAAAAAAAAAAAAAAAAAAAAAzXVD3/2QU//7j1L/+Y5R//eNUf/1jFD/+syy//////////////////fSvP/ohUz/5oNL/+SCSv/igUr/4IBJ/95/SP/cfkj/2nxH/9h7Rv/Wekb/1HlF/9J4RP/QdkT/znVD/8t0Qv/uiE6/AAAAAAAAAAAAAAAAAAAAAAAAAAC/bT6f+o9S//2QU//7j1L/+Y5R//aNUP/////////////////50r3/7IdN/+qGTP/ohEz/5oNL/+SCSv/igUr/4IBJ/95/SP/cfUj/2nxH/9d7Rv/VekX/03lF/9F3RP/PdkP/zXVD/+WDS/8AAAAAAAAAAAAAAAAAAAAAAAAAAL9tPmDuiE7//pFT//2QU//6j1L/+I5R//7q3v//////+tO9//CJTv/uiE7/7IdN/+qGTP/ohEz/5oNL/+SCSv/igUr/4IBJ/91+SP/bfUf/2XxH/9d7Rv/VekX/03lF/9F3RP/PdkP/3n9J/++JTjAAAAAAAAAAAAAAAAAAAAAAv20+MN9/Sf/+kVP//pFT//yQUv/6j1L/+I5R//iicf/0i1D/8opP//CJTv/uiE7/7IdN/+qFTP/ohEz/5oNL/+OCSv/hgUn/339J/91+SP/bfUf/2XxH/9d7Rv/VekX/03hF/9F3RP/Xe0b/7ohOcAAAAAAAAAAAAAAAAAAAAAAAAAAAzHRC7/6RU//+kVP//pFT//yQUv/6j1L/+I1R//aMUP/0i1D/8opP//CJTv/tiE3/64ZN/+mFTP/nhEv/5YNL/+OCSv/hgEn/339J/91+SP/bfUf/2XxH/9d7Rv/VeUX/03hF/9h7Rv/sh02AAAAAAAAAAAAAAAAAAAAAAAAAAAC/bT5gz3ZD/99/Sf/ff0n/339J/99/Sf/ef0n/3n9I/95/Sf/ef0j/3n9I/95/SP/ef0j/3n9I/95/SP/ef0j/3n9I/95/SP/ef0j/3n9I/95+SP/ef0j/3n5I/91/SP/dfkj/44JK/+uGTVAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAP////////////////////8AAAAfAAAAHwAAAA8AAAAPgAAAD4AAAA+AAAAHgAAAB4AAAAfAAAAHwAAAB8AAAAPAAAAD4AAAA+AAAAPgAAAB4AAAAeAAAAHwAAAB8AAAAfAAAADwAAAA+AAAAPgAAAD/////////////////////")
                $TrayIcon.Text = "Status Report" + "`n" + "• File Stage: None" + "`n" + "• Percent Done: None"
                $TrayIcon.Visible = $True
            }

        }

        # User Doing Nothing (Idle Mode)
        if (!([System.Console]::KeyAvailable)) {

            # Variable Error Detection
            if (($UserInputError -EQ $True) -AND ($LastKeyPress -EQ "F1")) {

                # Automatic [F1] Key Press
                [Void][System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
                [System.Windows.Forms.SendKeys]::SendWait("{F1}")

            }

            # Variable Error Detection
            if (($UserInputError -EQ $True) -AND ($LastKeyPress -EQ "F2")) {

                # Automatic [F2] Key Press
                [Void][System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
                [System.Windows.Forms.SendKeys]::SendWait("{F2}")

            }

            # Variable Error Detection
            if (($UserInputError -EQ $True) -AND ($LastKeyPress -EQ "F3")) {

                # Automatic [F3] Key Press
                [Void][System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
                [System.Windows.Forms.SendKeys]::SendWait("{F3}")

            }

            # Console Screen Refresh
            [System.Console]::Clear()

            # Console Line
            "—" * $Host.UI.RawUI.WindowSize.Width

            # Console Information
            Write-Host "Key Press Detection"
            Write-Host "Welcome to a script that will merge part files into the"
            Write-Host "original file. You can split a file using " -NoNewLine
            Write-Host "Split File Tool" -ForegroundColor "Yellow" -NoNewLine
            Write-Host "."
            Write-Host "PowerShell must be selected for [F1] - [F12] to operate."

            # Key Press [F1] Selection Information
            if ([String]::IsNullOrEmpty($TargetPath)) {
                Write-Host "• Press [F1] to set location of a part file." -ForegroundColor "Red"
            } else {
                Write-Host "• Press [F1] to set location of a part file."
            }

            # Key Press [F2] Selection Information
            if ([String]::IsNullOrEmpty($TargetDestination)) {
                Write-Host "• Press [F2] to set location of destination folder." -ForegroundColor "Red"
            } else {
                Write-Host "• Press [F2] to set location of destination folder."
            }

            # Key Press [F3] Selection Information
            if ([String]::IsNullOrEmpty($BufferSize)) {
                Write-Host "• Press [F3] to set size of memory buffer per part file." -ForegroundColor "Red"
            } else {
                Write-Host "• Press [F3] to set size of memory buffer per part file."
            }

            # Key Press [F4] Selection Information
            if (([String]::IsNullOrEmpty($DeleteMode)) -AND ([String]::IsNullOrEmpty($RecycleMode))) {
                Write-Host "• Press [F4] to mark part files for permanent deletion." -ForegroundColor "Red"
            } elseif (($DeleteMode -EQ $True) -AND ([String]::IsNullOrEmpty($RecycleMode))) {
                Write-Host "• Press [F4] to move marked files to " -NoNewLine
                Write-Host "Recycle Bin" -ForegroundColor "Yellow" -NoNewLine
                Write-Host "."
            } elseif (!([String]::IsNullOrEmpty($RecycleMode))) {
                Write-Host "• Press [F4] to unmark part files from deletion."
            }

            # Key Press [F5] Selection Information
            if ([String]::IsNullOrEmpty($ManifestMode)) {
                Write-Host "• Press [F5] to enable file integrity from manifest file." -ForegroundColor "Red"
            } else {
                Write-Host "• Press [F5] to disable file integrity from manifest file."
            }

            # Key Press [F6] Selection Information
            if ([String]::IsNullOrEmpty($ExampleMode)) {
                Write-Host "• Press [F6] to activate example mode." -ForegroundColor "Red"
            } else {
                Write-Host "• Press [F6] to deactivate example mode."
            }

            # Key Press [F7] Selection Information
            if ([String]::IsNullOrEmpty($TargetPath)) {
                Write-Host "• Press [F7] to activate script [part file required]." -ForegroundColor "Red"
            } else {
                Write-Host "• Press [F7] to execute merge file module."
            }

            if ($ShowError -EQ $True) {

                # Key Press [F12] Selection Information
                Write-Host "• Press [F12] to terminate running script." -ForegroundColor "Yellow"

            }

            # Console Line
            "—" * $Host.UI.RawUI.WindowSize.Width

            # Console Information
            Write-Host "Selected Information"

            # Key Press [F1] Variable Detection
            if (!([String]::IsNullOrEmpty($TargetPath))) {
                Write-Host "• File Path: $TargetPath" -ForegroundColor "Yellow"
            } else {
                Write-Host "• File Path: None"
            }

            # Key Press [F2] Variable Detection
            if (!([String]::IsNullOrEmpty($TargetDestination))) {
                Write-Host "• Destination: $TargetDestination" -ForegroundColor "Yellow"
            } else {
                Write-Host "• Destination: None"
            }

            # Key Press [F3] Variable Detection
            if (!([String]::IsNullOrEmpty($BufferSize))) {
                Write-Host "• Memory Buffer Size: $BufferSize [$($BufferSize/1MB) MB]" -ForegroundColor "Yellow"
            } else {
                Write-Host "• Memory Buffer Size: None"
            }

            # Key Press [F4] Variable Detection
            if ((!([String]::IsNullOrEmpty($DeleteMode))) -AND ([String]::IsNullOrEmpty($RecycleMode))) {
                Write-Host "• Source Deletion: Enabled" -ForegroundColor "Yellow"
            } elseif (([String]::IsNullOrEmpty($DeleteMode)) -AND (!([String]::IsNullOrEmpty($RecycleMode)))) {
                Write-Host "• Source Deletion: Recycling" -ForegroundColor "Yellow"
            } elseif (([String]::IsNullOrEmpty($DeleteMode)) -AND ([String]::IsNullOrEmpty($RecycleMode))) {
                Write-Host "• Source Deletion: Disabled"
            }

            # Key Press [F5] Variable Detection
            if (!([String]::IsNullOrEmpty($ManifestMode))) {
                Write-Host "• Manifest Mode: Enabled" -ForegroundColor "Yellow"
            } else {
                Write-Host "• Manifest Mode: Disabled"
            }

            # Key Press [F6] Variable Detection
            if (!([String]::IsNullOrEmpty($ExampleMode))) {
                Write-Host "• Example Mode: Enabled" -ForegroundColor "Yellow"
            } else {
                Write-Host "• Example Mode: Disabled"
            }

            # Required Variables are Entered
            if (!([String]::IsNullOrEmpty($TargetPath))) {
                Write-Host "• Execution Mode: Unlocked" -ForegroundColor "Yellow"
            } else {
                Write-Host "• Execution Mode: Locked"
            }

            # File Existance Validation
            if (!(([System.IO.FileInfo]$TargetPath).Exists)) {
                Remove-Variable -Name "TargetPath" -ErrorAction "SilentlyContinue"
            }

            # Folder Existance Validation
            if (!(([System.IO.DirectoryInfo]$TargetDestination).Exists)) {
                Remove-Variable -Name "TargetDestination" -ErrorAction "SilentlyContinue"
            }

            # Remove Example Mode
            if ($ExampleMode -EQ $True) {
                ForEach ($File in $ExampleList) {
                    if (!([System.IO.FileInfo]$File).Exists) {

                        # Refresh Variable
                        if ($TargetPath -EQ $ExampleFile) {
                            Remove-Variable -Name "TargetPath" -ErrorAction "SilentlyContinue"
                        }
                        if ($TargetDestination -EQ "$env:UserProfile\Desktop\Example Folder") {
                            Remove-Variable -Name "TargetDestination" -ErrorAction "SilentlyContinue"
                        }
                        if ($ExampleMode -EQ $True) {
                            Remove-Variable -Name "ExampleMode" -ErrorAction "SilentlyContinue"
                        }

                    }
                }
            }

            # Enable Example Mode
            if ([String]::IsNullOrEmpty($ExampleMode)) {
                if ((!([String]::IsNullOrEmpty($TargetPath))) -AND (!([String]::IsNullOrEmpty($TargetDestination)))) {
                    if ((($TargetPath -Match "Example File.bin.*.part") -AND (([System.IO.FileInfo]$TargetPath).Exists)) -AND (($TargetDestination -EQ "$env:UserProfile\Desktop\Example Folder") -AND (([System.IO.DirectoryInfo]$TargetDestination).Exists))) {

                        # Automatic [F5] Key Press
                        [Void][System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
                        [System.Windows.Forms.SendKeys]::SendWait("{F5}")

                    }
                }
            }

            # Console Line
            "—" * $Host.UI.RawUI.WindowSize.Width

            # Console Pause
            [Threading.Thread]::Sleep(500)

        }

        # Key Press Detection
        if ([System.Console]::KeyAvailable) {

            # User Pressing Defined Key
            switch ([System.Console]::ReadKey().Key) {

                "F1" {

                    # Define Last Key
                    $LastKeyPress = "F1"

                    # Console Screen Refresh
                    [System.Console]::Clear()

                    # Console Line
                    "—" * $Host.UI.RawUI.WindowSize.Width

                    # Console Information
                    Write-Host "User Action Required"
                    Write-Host "Please enter the location of any part file"
                    Write-Host "that will then be merged into the original."
                    Write-Host "• $env:UserProfile\Desktop\Example File.txt.001.part"
                    Write-Host "• $env:UserProfile\Desktop\Install Disc.iso.050.part"

                    # Variable Error Detection
                    if ($UserInputError -EQ $True) {

                        # Error Mode Refresh
                        Remove-Variable -Name "UserInputError" -ErrorAction "SilentlyContinue"

                        # Console Line
                        "—" * $Host.UI.RawUI.WindowSize.Width

                        # Console Information
                        Write-Host "Warning Notification" -ForegroundColor "Yellow"
                        Write-Host "You have entered a part file path that doesn't exist." -ForegroundColor "Yellow"
                        Write-Host "Please enter the full path of an existing part file." -ForegroundColor "Yellow"

                    }

                    # Console Line
                    "—" * $Host.UI.RawUI.WindowSize.Width

                    # Grab User Input as Variable
                    $TargetPath = Read-Host -Prompt "Select File Path"

                    # User Input is Empty Variable
                    if ([String]::IsNullOrWhiteSpace($TargetPath)) {
                        Remove-Variable -Name "TargetPath" -ErrorAction "SilentlyContinue"
                    }

                    # User Input is Not Empty Variable
                    if (!([String]::IsNullOrWhiteSpace($TargetPath))) {

                        # Expand User Input Variable
                        $TargetPath = [System.IO.Path]::GetFullPath($ExecutionContext.InvokeCommand.ExpandString($TargetPath))

                        # Set Current Directory as Default
                        if ($TargetPath.ToLower().Contains("$env:SystemRoot\System32".ToLower())) {
                            $TargetPath = [System.IO.Path]::GetFullPath($ExecutionContext.InvokeCommand.ExpandString($PWD.Path)) + "\" + [System.IO.Path]::GetFileName($TargetPath)
                        }

                        # String Format Validation
                        if (!($TargetPath -Match "\\")) {
                            Remove-Variable -Name "TargetPath" -ErrorAction "SilentlyContinue"
                            $UserInputError = $True
                        }

                        # File Path Validation
                        if (!(([System.IO.Path]::GetFileName($TargetPath) -Match [Regex]::New("^[a-zA-Z0-9\ \:\'\-\[\]{}()+=_.,;@~#!£$€¥%^&×¤]+$")))) {
                            Remove-Variable -Name "TargetPath" -ErrorAction "SilentlyContinue"
                            $UserInputError = $True
                        }

                        # File Existance Validation
                        if (!(([System.IO.FileInfo]$TargetPath).Exists)) {
                            Remove-Variable -Name "TargetPath" -ErrorAction "SilentlyContinue"
                            $UserInputError = $True
                        }

                        # Invalid Extension Validation
                        if ([System.IO.Path]::GetExtension($TargetPath) -NotMatch ".part") {
                            Remove-Variable -Name "TargetPath" -ErrorAction "SilentlyContinue"
                            $UserInputError = $True
                        }

                    }

                }

                "F2" {

                    # Define Last Key
                    $LastKeyPress = "F2"

                    # Console Screen Refresh
                    [System.Console]::Clear()

                    # Console Line
                    "—" * $Host.UI.RawUI.WindowSize.Width

                    # Console Information
                    Write-Host "User Action Required"
                    Write-Host "Please enter the location of a folder that"
                    Write-Host "will host the original file being generated."
                    Write-Host "Default value of this is parent of file path."
                    Write-Host "• $env:UserProfile\Desktop\Example Folder"
                    Write-Host "• $env:UserProfile\Desktop\Welcome Folder"

                    # Variable Error Detection
                    if ($UserInputError -EQ $True) {

                        # Error Mode Refresh
                        Remove-Variable -Name "UserInputError" -ErrorAction "SilentlyContinue"

                        # Console Line
                        "—" * $Host.UI.RawUI.WindowSize.Width

                        # Console Information
                        Write-Host "Warning Notification" -ForegroundColor "Yellow"
                        Write-Host "You have entered an invalid directory path format." -ForegroundColor "Yellow"
                        Write-Host "Please enter a directory path that could exist." -ForegroundColor "Yellow"

                    }

                    # Console Line
                    "—" * $Host.UI.RawUI.WindowSize.Width

                    # Grab User Input as Variable
                    $TargetDestination = Read-Host -Prompt "Select Destination"

                    # User Input is Empty Variable
                    if ([String]::IsNullOrWhiteSpace($TargetDestination)) {
                        Remove-Variable -Name "TargetDestination" -ErrorAction "SilentlyContinue"
                    }

                     # User Input is Not Empty Variable
                    if (!([String]::IsNullOrWhiteSpace($TargetDestination))) {

                        # Expand User Input Variable
                        $TargetDestination = [System.IO.Path]::GetFullPath($ExecutionContext.InvokeCommand.ExpandString($TargetDestination))

                        # Set Current Directory as Default
                        if ($TargetDestination.ToLower().Contains("$env:SystemRoot\System32".ToLower())) {
                            $TargetDestination = [System.IO.Path]::GetFullPath($ExecutionContext.InvokeCommand.ExpandString($PWD.Path)) + "\" + (Split-Path -Path $TargetDestination -Leaf)
                        }

                        # Folder Path Validation
                        if (!(($TargetDestination -Match [Regex]::New("^[a-zA-Z0-9\ \:\'\-\[\\\]{}()+=_.,;@~#!£$€¥%^&×¤]+$")) -AND (!($TargetDestination.Contains("\\"))))) {
                            Remove-Variable -Name "TargetDestination" -ErrorAction "SilentlyContinue"
                            $UserInputError = $True
                        }

                        # Folder Existance Validation
                        if ((!(([System.IO.DirectoryInfo]$TargetDestination).Exists)) -AND (!($UserInputError -EQ $True))) {
                            Remove-Variable -Name "TerminateSelection" -ErrorAction "SilentlyContinue"
                            while (!($TerminateSelection)) {

                                # Console Screen Refresh
                                [System.Console]::Clear()

                                # Console Line
                                "—" * $Host.UI.RawUI.WindowSize.Width

                                # Console Information
                                Write-Host "User Action Required"
                                Write-Host "You have entered a folder path that doesn't exist."
                                Write-Host "Please check the desired path and select an option."
                                Write-Host "• Entered Path: $TargetDestination"
                                Write-Host "• Press [F1] to create new folder."
                                Write-Host "• Press [F2] to ignore and continue."

                                # Console Line
                                "—" * $Host.UI.RawUI.WindowSize.Width

                                # Console Information
                                Write-Host "Warning Notification" -ForegroundColor "Yellow"
                                Write-Host "You can not execute the final [F7] operation if the" -ForegroundColor "Yellow"
                                Write-Host "destination folder does not exist at present moment." -ForegroundColor "Yellow"

                                # Console Line
                                "—" * $Host.UI.RawUI.WindowSize.Width

                                # User Deterime an Option
                                switch ([System.Console]::ReadKey().Key) {
                                    "F1" {
                                        [Void][System.IO.Directory]::CreateDirectory($TargetDestination)
                                        $TerminateSelection = $True
                                    }
                                    "F2" {
                                        Remove-Variable -Name "TargetDestination" -ErrorAction "SilentlyContinue"
                                        $TerminateSelection = $True
                                    }
                                    "ENTER" {
                                        [Void][System.IO.Directory]::CreateDirectory($TargetDestination)
                                        $TerminateSelection = $True
                                    }
                                    "BACKSPACE" {
                                        Remove-Variable -Name "TargetDestination" -ErrorAction "SilentlyContinue"
                                        $TerminateSelection = $True
                                    }
                                }

                                # Console Pause
                                [Threading.Thread]::Sleep(500)

                            }
                        }

                    }

                }

                "F3" {

                    # Define Last Key
                    $LastKeyPress = "F3"

                    # Console Screen Refresh
                    [System.Console]::Clear()

                    # Console Information
                    Write-Host "User Action Required"
                    Write-Host "Please enter the desired bytes of the memory"
                    Write-Host "buffer. Recommended to leave at default value."
                    Write-Host "Default value of this is set to 4194304 [4 MB]."

                    # Variable Error Detection
                    if ($UserInputError -EQ $True) {

                        # Error Mode Refresh
                        Remove-Variable -Name "UserInputError" -ErrorAction "SilentlyContinue"

                        # Console Line
                        "—" * $Host.UI.RawUI.WindowSize.Width

                        # Console Information
                        Write-Host "Warning Notification" -ForegroundColor "Yellow"
                        Write-Host "You have selected an incorrect value." -ForegroundColor "Yellow"
                        Write-Host "Please enter a number greater than zero." -ForegroundColor "Yellow"

                    }

                    # Console Line
                    "—" * $Host.UI.RawUI.WindowSize.Width

                    # Grab User Input as Variable
                    $BufferSize = Read-Host -Prompt "Select Buffer Size"

                    # User Input is Empty Variable
                    if ([String]::IsNullOrWhiteSpace($BufferSize)) {
                        Remove-Variable -Name "BufferSize" -ErrorAction "SilentlyContinue"
                    }

                    # User Input is Not Empty Variable
                    if (!([String]::IsNullOrWhiteSpace($BufferSize))) {

                        # Expand Variable to Bytes
                        $BufferSize = 1 * $BufferSize

                        if (($BufferSize -LT 1) -OR (!($BufferSize -Match "^[0-9]*$"))) {
                            Remove-Variable -Name "BufferSize" -ErrorAction "SilentlyContinue"
                            $UserInputError = $True
                        }

                    }

                }

                "F4" {

                    # Toggle Variable
                    if (([String]::IsNullOrEmpty($DeleteMode)) -AND ([String]::IsNullOrEmpty($RecycleMode))) {
                        $DeleteMode = $True
                    } elseif (($DeleteMode -EQ $True) -AND ([String]::IsNullOrEmpty($RecycleMode))) {
                        Remove-Variable -Name "DeleteMode" -ErrorAction "SilentlyContinue"
                        $RecycleMode = $True
                    } elseif (!([String]::IsNullOrEmpty($RecycleMode))) {
                        Remove-Variable -Name "RecycleMode" -ErrorAction "SilentlyContinue"
                    }

                }

                "F5" {

                    # Toggle Variable
                    if ([String]::IsNullOrEmpty($ManifestMode)) {
                        $ManifestMode = $True
                    } else {
                        Remove-Variable -Name "ManifestMode" -ErrorAction "SilentlyContinue"
                    }

                }

                "F6" {

                    # Enable Example Mode
                    if ([String]::IsNullOrEmpty($ExampleMode)) {

                        # Define Example File List
                        $ExampleList = [System.Collections.Generic.List[Object]]::New()

                        # Example File Amount
                        $FileCount = 20

                        while ($FileCount -NE 0) {

                            # Define Part Syntax
                            [String]$FilePart = $FileCount
                            $FilePart = $FilePart.PadLeft(3,"0")

                            # Extend Array List
                            [Void]$ExampleList.Add("$env:UserProfile\Desktop\Example Folder\Example File.bin.$FilePart.part")
                            $FileCount = $FileCount - 1

                        }

                        # Create Folder
                        if (!(([System.IO.DirectoryInfo]"$env:UserProfile\Desktop\Example Folder").Exists)) {
                            [Void][System.IO.Directory]::CreateDirectory("$env:UserProfile\Desktop\Example Folder")
                        }

                        # Create File
                        ForEach ($File in $ExampleList) {
                            if (!(([System.IO.FileInfo]$File).Exists)) {
                                $FileName = [System.IO.Path]::GetFileName($File)
                                $FilePath = [System.IO.Path]::GetDirectoryName($File)
                                $Element = New-Item -Path $FilePath -Name $FileName -ItemType "File"
                                $Stream = $Element.OpenWrite()
                                $Stream.SetLength(20MB)
                                $Stream.Flush()
                                $Stream.Close()
                            }
                        }

                        # Define Variable
                        if ([String]::IsNullOrEmpty($TargetPath)) {
                            $ExampleFile = Get-Random -InputObject $ExampleList -Count 1
                            $TargetPath = $ExampleFile
                        }
                        if ([String]::IsNullOrEmpty($TargetDestination)) {
                            $TargetDestination = "$env:UserProfile\Desktop\Example Folder"
                        }
                        if ([String]::IsNullOrEmpty($BufferSize)) {
                            $BufferSize = 4MB
                        }
                        if ([String]::IsNullOrEmpty($ExampleMode)) {
                            $ExampleMode = $True
                        }

                    # Disable Example Mode
                    } else {

                        # Remove Example File
                        ForEach ($File in $ExampleList) {
                            Remove-Item -Path $File -Force
                        }

                        # Empty Folder Checker
                        if (([System.IO.Directory]::GetFiles("$env:UserProfile\Desktop\Example Folder").Count) -EQ 0) {

                            # Remove Example Folder
                            Remove-Item -Path "$env:UserProfile\Desktop\Example Folder" -Force

                        }

                        # Refresh Variable
                        if ($TargetPath -EQ $ExampleFile) {
                            Remove-Variable -Name "TargetPath" -ErrorAction "SilentlyContinue"
                        }
                        if ($TargetDestination -EQ "$env:UserProfile\Desktop\Example Folder") {
                            Remove-Variable -Name "TargetDestination" -ErrorAction "SilentlyContinue"
                        }
                        if ($BufferSize -EQ 4MB) {
                            Remove-Variable -Name "BufferSize" -ErrorAction "SilentlyContinue"
                        }
                        if ($ExampleMode -EQ $True) {
                            Remove-Variable -Name "ExampleMode" -ErrorAction "SilentlyContinue"
                        }

                    }

                }

                "F7" {

                    # Activate Running Mode
                    if (!([String]::IsNullOrEmpty($TargetPath))) {
                        $RunningMode = $True
                    }

                }

                "F12" {

                    # System Tray Icon Removal
                    $TrayIcon.Visible = $False

                    # Define Variable
                    $ExitMode = $True

                }

                { $True } {

                    # Debugging Mode
                    if (($ShowError -EQ $True) -AND ($Error[0])) {
                        $ElementCounter = 0
                        ForEach ($Element in $Error) {
                            Write-Host $Error[$ElementCounter] -ForegroundColor "Red" -BackgroundColor "Black"
                            $ElementCounter = $ElementCounter + 1
                        }
                        $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown") | Out-Null
                        $Error.Clear()
                    }

                }

            }

        }

    }

    while (($RunningMode -EQ $True) -AND (!($ExitMode -EQ $True))) {

        if (!($ProgramExecuted -EQ $True)) {

            # Define Variable
            $ProgramExecuted = $True

            # System Tray Message
            $TrayIcon.Text = "Status Report" + "`n" + "• File Stage: Loading" + "`n" + "• Percent Done: Loading"

            # Console Information
            Write-Host "Initalising PowerShell"
            Write-Host "• Key [F7] has been pressed."
            Write-Host "• Please wait for operation to start."

            # Console Line
            "—" * $Host.UI.RawUI.WindowSize.Width

            # Define Array List
            $ErrorList = [System.Collections.ArrayList]::New()
            $PartList = [System.Collections.ArrayList]::New()

            # Target Variable Definition
            $TargetPath = $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath($TargetPath)
            $TargetFile = [System.IO.Path]::GetFileName($TargetPath)
            $TargetName = [System.IO.Path]::GetFileNameWithoutExtension($TargetPath)
            $TargetExtension = [System.IO.Path]::GetExtension($TargetPath)
            $TargetSize = ([System.IO.FileInfo]$TargetPath).Length
            $TargetParent = [System.IO.Path]::GetDirectoryName($TargetPath)

            # Define Default Variables
            if ([String]::IsNullOrEmpty($TargetSize)) {
                $TargetSize = 0
            }
            if (([String]::IsNullOrEmpty($BufferSize)) -OR ($BufferSize -EQ 0)) {
                $BufferSize = 4194304
            }
            if ([String]::IsNullOrEmpty($TargetDestination)) {
                $TargetDestination = $TargetParent
            }

            # Target Variables Error Detection
            if ([String]::IsNullOrEmpty($TargetPath)) {
                [Void]$ErrorList.Add("SourceMissing")
            }
            if (!(([System.IO.FileInfo]$TargetPath).Exists)) {
                [Void]$ErrorList.Add("TargetMissing")
            }
            if ((([System.IO.FileInfo]$TargetPath).Exists) -AND (([System.IO.FileInfo]$TargetPath).Length -EQ 0)) {
                [Void]$ErrorList.Add("TargetEmpty")
            }

            # Create Target Directory if Non-Existant
            if (!(([System.IO.DirectoryInfo]$TargetDestination).Exists)) {
                [Void][System.IO.Directory]::CreateDirectory($TargetDestination)
            }

            # Part File Variable Definition
            $SourceFile = [System.IO.Path]::GetFileNameWithoutExtension($TargetPath) -Replace "\.\d+$"
            $PartFragment = [System.IO.Path]::GetFileNameWithoutExtension($TargetPath) -Replace ".+\."
            $PartLength = $PartFragment.Length

            # Manifest File Definition
            if ($ManifestMode -EQ $True) {
                $ManifestFile = "$TargetParent\$SourceFile.json"
            }

            # Part File List Variable Definition
            Get-ChildItem $TargetParent | Where-Object { ($_.Name -Match "\.(\d{$PartLength})\.part") -AND ($_.Name -Match "$SourceFile.*.part") } | Sort-Object -Property "Name" | ForEach-Object {

                # Set Variable Once
                if ([String]::IsNullOrEmpty($PartNumberMin)) {
                    $PartNumberMin = [System.IO.Path]::GetFileNameWithoutExtension($_.FullName) -Replace ".+\."
                }

                # Define Variable
                $PartNumberMax = [System.IO.Path]::GetFileNameWithoutExtension($_.FullName) -Replace ".+\."

                # Variable Error Detection
                if ([String]::IsNullOrEmpty($PartNumberMin)) {
                    $PartNumberMin = 0
                }
                if ([String]::IsNullOrEmpty($PartNumberMax)) {
                    $PartNumberMax = 0
                }

                # Default Object Variable Definition
                $ElementName = $_.Name
                $ElementPath = $_.FullName
                $ElementSize = $_.Length

                # Manifest Object Variable Definition
                if ($ManifestMode -EQ $True) {
                    $ElementHash = Get-FileHash $_.FullName -Algorithm "SHA512" | Select-Object -ExpandProperty "Hash"
                    if ($_.Name -Match "\.(\d+)\.part") {
                        $ElementNode = [Int]$Matches[1]
                    }
                }

                # Calculate Total Size of Part Files
                $PartBytesTotal = $PartBytesTotal + $ElementSize

                # Variable Error Detection
                if ([String]::IsNullOrEmpty($PartBytesTotal)) {
                    $PartBytesTotal = 0
                }

                # Manifest Mode Detection
                if ($ManifestMode -EQ $True) {

                    # Define Part Object
                    $PartObject = [PSCustomObject]@{
                        "Node" = $ElementNode
                        "Name" = $ElementName
                        "Path" = $ElementPath
                        "Hash" = $ElementHash
                        "Bytes" = $ElementSize
                    }

                    # Extend Array List
                    [Void]$PartList.Add($PartObject)

                } else {

                    # Define Part Object
                    $PartObject = [PSCustomObject]@{
                        "Name" = $ElementName
                        "Path" = $ElementPath
                        "Bytes" = $ElementSize
                    }

                    # Extend Array List
                    [Void]$PartList.Add($PartObject)

                }

            }

            # Memory Variable Definition
            $DriveLetter = [System.IO.Path]::GetPathRoot($TargetPath)[0]
            $DriveRemaining = Get-Volume -DriveLetter $DriveLetter | Select-Object -ExpandProperty "SizeRemaining"

            # Memory Variable Error Detection
            if ($DriveRemaining -LT $PartBytesTotal) {
                [Void]$ErrorList.Add("StorageRemaining")
            }

            # Console Screen Refresh
            [System.Console]::Clear()

            # Console Line
            "—" * $Host.UI.RawUI.WindowSize.Width

            # Console Information
            Write-Host "Variable Information"
            Write-Host "Target Path: $TargetPath"
            Write-Host "Target File: $TargetFile"
            Write-Host "Target Name: $TargetName"
            Write-Host "Target Extension: $TargetExtension"
            Write-Host "Target Size: $TargetSize [$($TargetSize/1MB) MB]"
            Write-Host "Target Parent: $TargetParent"
            Write-Host "Selected Destination: $TargetDestination"
            Write-Host "Storage Remaning: $DriveRemaining [$($DriveRemaining/1MB) MB]"
            Write-Host "Buffer Size: $BufferSize [$($BufferSize/1MB) MB]"

            # Console Line
            "—" * $Host.UI.RawUI.WindowSize.Width

            # Console Information
            Write-Host "Source Information"
            Write-Host "File Name: $SourceFile"
            Write-Host "File Amount: $($PartList.Count) [$PartNumberMin - $PartNumberMax]"
            Write-Host "File Size: $PartBytesTotal [$($PartBytesTotal/1MB) MB]"

            # Console Line
            "—" * $Host.UI.RawUI.WindowSize.Width

            # Console Information
            Write-Host "Part File List"
            ForEach ($Element in $PartList) {
                Write-Host "• $($Element.Name)"
            }

            # Console Line
            "—" * $Host.UI.RawUI.WindowSize.Width

            # Element Variable Definition
            [Int]$ElementPartNumber = $PartNumberMin
            $ElementPaddingLength = $PartFragment.ToString().Length
            $MissingList = [System.Collections.Generic.List[Object]]::New()

            # Missing Part Files Validation
            [Int]$PartNumberMin..[Int]$PartNumberMax | ForEach-Object {
                $ElementPartFormatted = [String]::Format("{0:D$ElementPaddingLength}", [Int]$ElementPartNumber)
                if (!(([System.IO.FileInfo]"$TargetParent\$SourceFile.$ElementPartFormatted.part").Exists)) {
                    [Void]$MissingList.Add("$TargetParent\$SourceFile.$ElementPartFormatted.part")
                }
                $ElementPartNumber = $ElementPartNumber + 1
            }

            if (!([String]::IsNullOrEmpty($MissingList))) {

                # Console Information
                Write-Host "Warning Announcement" -ForegroundColor "Yellow"
                Write-Host "Please note that after performing validation for the existance" -ForegroundColor "Yellow"
                Write-Host "of each part file the following part files could not be found." -ForegroundColor "Yellow"
                ForEach ($File in $MissingList) {
                    Write-Host "• $File" -ForegroundColor "Yellow"
                }
                Write-Host "• Press [Enter] to continue with execution." -ForegroundColor "Yellow"

                # Await User Input
                $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown") | Out-Null

                # Console Line
                "—" * $Host.UI.RawUI.WindowSize.Width

            }

            if (($ManifestMode -EQ $True) -AND (([System.IO.FileInfo]$ManifestFile).Exists)) {

                # Define Variable
                $ManifestData = Get-Content -Path $ManifestFile -Raw -Encoding UTF8 | ConvertFrom-Json
                $MismatchList = [System.Collections.Generic.List[Object]]::New()

                # Manifest File Checker
                $ManifestData.PSObject.Properties | ForEach-Object {
                    if ($_.Name -NE "Master") {
                        ForEach ($Element in $PartList) {
                            if ($Element.Node -EQ $_.Name) {

                                # Path Mismatch Detection
                                # if (!($Element.Path -EQ $_.PSObject.Properties.Value.Path)) {
                                #     [Void]$MismatchList.Add("• Path Mismatch Detected [Part $($Element.Node)]")
                                #     [Void]$MismatchList.Add("• Manifest File: $($_.PSObject.Properties.Value.Path)")
                                #     [Void]$MismatchList.Add("• Part List: $($Element.Path)")
                                # }

                                # Hash Mismatch Detection
                                if (!($Element.Hash -EQ $_.PSObject.Properties.Value.Hash)) {
                                    [Void]$MismatchList.Add("• Hash Mismatch Detected [Part $($Element.Node)]")
                                    [Void]$MismatchList.Add("• Manifest File: $($_.PSObject.Properties.Value.Hash.SubString(0,($Host.UI.RawUI.WindowSize.Width - 17)))")
                                    [Void]$MismatchList.Add("• Part List: $($Element.Hash.SubString(0,($Host.UI.RawUI.WindowSize.Width - 13)))")
                                    [Void]$MismatchList.Add("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
                                }

                                # Bytes Mismatch Detection
                                if (!($Element.Bytes -EQ $_.PSObject.Properties.Value.Bytes)) {
                                    [Void]$MismatchList.Add("• Bytes Mismatch Detected [Part $($Element.Node)]")
                                    [Void]$MismatchList.Add("• Manifest File: $($_.PSObject.Properties.Value.Bytes) [$($_.PSObject.Properties.Value.Bytes/1MB) MB]")
                                    [Void]$MismatchList.Add("• Part List: $($Element.Bytes) [$($Element.Bytes/1MB) MB]")
                                    [Void]$MismatchList.Add("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
                                }

                            }
                        }
                    }
                }

                if (!([String]::IsNullOrEmpty($MismatchList))) {

                    # Console Information
                    Write-Host "Warning Announcement" -ForegroundColor "Yellow"
                    Write-Host "Please note that after comparing the manifest file data and the" -ForegroundColor "Yellow"
                    Write-Host "calculated part file list the following mismatches have occured." -ForegroundColor "Yellow"
                    Write-Host "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" -ForegroundColor "Yellow"
                    ForEach ($Element in $MismatchList) {
                        Write-Host "$Element" -ForegroundColor "Yellow"
                    }
                    Write-Host "• Press [Enter] to continue with execution." -ForegroundColor "Yellow"

                    # Await User Input
                    $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown") | Out-Null

                    # Console Line
                    "—" * $Host.UI.RawUI.WindowSize.Width

                }

            }

            # Show Error List
            if ($ErrorList[0]) {

                # Console Information
                Write-Host "Unresolved Error List"
                Write-Host "Please fix the following errors before continuing."
                Write-Host "Failure to fix these errors will result in errors."
                ForEach ($Element in $ErrorList) {
                    if ($Element.Contains("SourceMissing")) {
                        Write-Host "• You have not defined the target file." -ForegroundColor "Red"
                    }
                    if ($Element.Contains("TargetMissing")) {
                        Write-Host "• You have selected a file which does not exist." -ForegroundColor "Red"
                    }
                    if ($Element.Contains("TargetEmpty")) {
                        Write-Host "• You have selected an empty file as the target path." -ForegroundColor "Red"
                    }
                    if ($Element.Contains("StorageRemaining")) {
                        Write-Host "• You do not have enough memory storage available." -ForegroundColor "Red"
                    }
                }

                # Console Line
                "—" * $Host.UI.RawUI.WindowSize.Width

            }

            # Error Detection
            if ([String]::IsNullOrEmpty($ErrorList)) {

                # Create Output Stream for Reassembled File
                $SourcePath = [String]::Format("$TargetDestination\$SourceFile")
                $OutputStream = [System.IO.File]::Open($SourcePath, "Create")

                # Define Variable
                $StreamLog = 0
                $TotalBytesLoaded = 0
                $TotalBytesRemaining = 0

                $PartList | ForEach-Object {

                    # Load Part File
                    $Reader = [IO.File]::OpenRead($_.Path)
                    $Reader.CopyTo($OutputStream, $BufferSize)
                    $Reader.Close()

                    # Increment Variable
                    $StreamLog = $StreamLog + 1
                    $PartFileLoaded = $PartFileLoaded + 1
                    $TotalBytesLoaded = $TotalBytesLoaded + $_.Bytes
                    $TotalBytesRemaining = $PartBytesTotal - $TotalBytesLoaded
                    $PercentComplete = [Math]::Round((($TotalBytesLoaded / $PartBytesTotal) * 100), 2) | ForEach {$_.ToString("N2")}

                    # System Tray Message
                    $TrayIcon.Text = "Status Report" + "`n" + "• File Stage: $PartFileLoaded/$($PartList.Count)" + "`n" + "• Percent Done: $([Math]::Round($PercentComplete))%"

                    # Console Information
                    Write-Host "Stream Log [$StreamLog]"
                    Write-Host "• Part Path: $($_.Path)"
                    Write-Host "• Part File: $($_.Name)"
                    Write-Host "• Part Bytes: $($_.Bytes) [$($_.Bytes/1MB) MB]"
                    Write-Host "• Total Bytes Remaining: $TotalBytesRemaining [$($TotalBytesRemaining/1MB) MB]"
                    Write-Host "• Total Bytes Loaded: $TotalBytesLoaded [$($TotalBytesLoaded/1MB) MB]"
                    Write-Host "• Percent Complete: $PercentComplete"

                    # Console Line
                    "—" * $Host.UI.RawUI.WindowSize.Width

                    # Declare Slow Mode
                    $SlowMode = $False
                    $Milliseconds = 2500
                    if ($SlowMode -EQ $True) {
                        [Threading.Thread]::Sleep($Milliseconds)
                    }

                }

                # Exit Output Stream
                $OutputStream.Flush()
                $OutputStream.Close()

                if (($ManifestMode -EQ $True) -AND (([System.IO.FileInfo]$ManifestFile).Exists)) {

                    # Grab Source Hash
                    $SourceHash = Get-FileHash $SourcePath -Algorithm "SHA512" | Select-Object -ExpandProperty "Hash"

                    # Define Variable
                    $ValidationList = [System.Collections.Generic.List[Object]]::New()

                    # Master File Checker
                    $ManifestData.PSObject.Properties | Where-Object {$_.Name -EQ "Master"} | ForEach-Object {

                        # Hash Mismatch Detection
                        if ($_.PSObject.Properties.Value.Hash -NE $SourceHash) {
                            [Void]$ValidationList.Add("• Hash Mismatch Detected [$([System.IO.Path]::GetFileName($SourcePath))]")
                            [Void]$ValidationList.Add("• Manifest File: $($_.PSObject.Properties.Value.Hash.SubString(0,($Host.UI.RawUI.WindowSize.Width - 17)))")
                            [Void]$ValidationList.Add("• Generated File: $($SourceHash.SubString(0,($Host.UI.RawUI.WindowSize.Width - 18)))")
                            [Void]$ValidationList.Add("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
                        }

                        # Bytes Mismatch Detection
                        if ($_.PSObject.Properties.Value.Bytes -NE $TotalBytesLoaded) {
                            [Void]$ValidationList.Add("• Bytes Mismatch Detected [$([System.IO.Path]::GetFileName($SourcePath))]")
                            [Void]$ValidationList.Add("• Manifest File: $($_.PSObject.Properties.Value.Bytes) [$($_.PSObject.Properties.Value.Bytes/1MB) MB]")
                            [Void]$ValidationList.Add("• Generated File: $TotalBytesLoaded [$($TotalBytesLoaded/1MB) MB]")
                            [Void]$ValidationList.Add("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -")
                        }

                    }

                    if (!([String]::IsNullOrEmpty($ValidationList))) {

                        # Console Information
                        Write-Host "Warning Announcement" -ForegroundColor "Yellow"
                        Write-Host "Please note that after comparing the original file and the" -ForegroundColor "Yellow"
                        Write-Host "generated file from the part files mismatches have occured." -ForegroundColor "Yellow"
                        Write-Host "• File Selected: $SourcePath" -ForegroundColor "Yellow"
                        Write-Host "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -" -ForegroundColor "Yellow"
                        ForEach ($Element in $ValidationList) {
                            Write-Host "$Element" -ForegroundColor "Yellow"
                        }
                        Write-Host "• Press [Enter] to ignore and continue." -ForegroundColor "Yellow"

                        # Await User Input
                        $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown") | Out-Null

                        # Console Line
                        "—" * $Host.UI.RawUI.WindowSize.Width

                    }

                }

                # Delete Part File
                if (($DeleteMode -EQ $True) -OR ($RecycleMode -EQ $True)) {

                    $PartList | ForEach-Object {

                        if ([String]::IsNullOrEmpty($DeletePrompt)) {

                            # Define Variable
                            $DeletePrompt = $True

                            # Console Information
                            $DeleteExample = $SourceFile + ".[000].part"
                            Write-Host "Deletion Announcement" -ForegroundColor "Yellow"
                            Write-Host "Please note that the part files have been marked for deletion." -ForegroundColor "Yellow"
                            Write-Host "You can exit the program now to prevent the deletion of files." -ForegroundColor "Yellow"
                            Write-Host "• File Selected: $DeleteExample [$($PartList.Count) Total] [$PartNumberMin - $PartNumberMax]" -ForegroundColor "Yellow"
                            Write-Host "• Press [Enter] to continue with deletion." -ForegroundColor "Yellow"

                            # Await User Input
                            $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown") | Out-Null

                        }

                        # Delete Part File
                        if ($DeleteMode -EQ $True) {
                            Remove-Item -Path $_.Path -Force
                        }

                        # Move Part File to Recycle Bin
                        if ($RecycleMode -EQ $True) {
                            ((New-Object -ComObject "Shell.Application").Namespace(0).ParseName($_.Path)).InvokeVerb("Delete")
                        }

                        # Console Information
                        Write-Host "• $($_.Name)" -ForegroundColor "Yellow"

                    }

                    # Delete Manifest File
                    if ($DeleteMode -EQ $True) {
                        Remove-Item -Path $ManifestFile -Force
                    }

                    # Move Manifest File to Recycle Bin
                    if ($RecycleMode -EQ $True) {
                        ((New-Object -ComObject "Shell.Application").Namespace(0).ParseName($ManifestFile)).InvokeVerb("Delete")
                    }

                    # Console Line
                    "—" * $Host.UI.RawUI.WindowSize.Width

                    # Console Pause
                    [Threading.Thread]::Sleep(500)

                }

                # Debugging Mode
                if (($ShowError -EQ $True) -AND ($Error[0])) {

                    # Console Information
                    Write-Host "Generated Error List"
                    Write-Host "Please note that the following errors have occured."
                    Write-Host "Failure to fix these issues will result in issues."
                    Write-Host "• Press [Enter] to continue."
                    ForEach ($Element in $Error) {
                        Write-Host "• $Element" -ForegroundColor "Red"
                    }

                    # Console Line
                    "—" * $Host.UI.RawUI.WindowSize.Width

                    # Console Pause
                    $Host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown") | Out-Null

                }

            }

            while (!($MarkedComplete -EQ $True)) {

                if (!($ShownMessage -EQ $True)) {

                    if ($ErrorList[0]) {

                        # Console Information
                        Write-Host "Operation Failed" -ForegroundColor "Red"
                        Write-Host "• Press [F1] to return to main menu." -ForegroundColor "Red"
                        Write-Host "• Press [F2] to close the terminal." -ForegroundColor "Red"

                        # Console Line
                        "—" * $Host.UI.RawUI.WindowSize.Width

                    }

                    if (!($ErrorList[0])) {

                        # Console Information
                        Write-Host "Operation Finished"
                        Write-Host "• Press [F1] to return to main menu."
                        Write-Host "• Press [F2] to close the terminal."

                        # Console Line
                        "—" * $Host.UI.RawUI.WindowSize.Width

                    }

                    # Define Variable
                    $ShownMessage = $True

                }

                if ([Console]::KeyAvailable) {

                    switch ([System.Console]::ReadKey().Key) {

                        "F1" {

                            # System Tray Message
                            $TrayIcon.Text = "Status Report" + "`n" + "• File Stage: None" + "`n" + "• Percent Done: None"

                            # Return to Main Menu
                            Remove-Variable -Name "RunningMode" -ErrorAction "SilentlyContinue"
                            Remove-Variable -Name "ProgramExecuted" -ErrorAction "SilentlyContinue"
                            $MarkedComplete = $True

                            # Master Variable Refresh
                            $PowerShell = [PowerShell]::Create()
                            $PowerShell.AddScript('Get-Variable | Select-Object -ExpandProperty "Name"') | Out-Null
                            $VariableList = $PowerShell.Invoke()
                            $PowerShell.Dispose()
                            $VariableList = Get-Variable | Select-Object -ExpandProperty "Name" | Where-Object { $VariableList -NotContains $_ }

                            # Define Ignore List
                            $ExemptionList = [System.Collections.Generic.List[Object]]::New()
                            [Void]$ExemptionList.Add("ErrorActionPreference")
                            [Void]$ExemptionList.Add("OutputEncoding")
                            [Void]$ExemptionList.Add("VariableList")
                            [Void]$ExemptionList.Add("ExemptionList")
                            [Void]$ExemptionList.Add("MarkedComplete")
                            [Void]$ExemptionList.Add("TargetPath")
                            [Void]$ExemptionList.Add("TargetDestination")
                            [Void]$ExemptionList.Add("BufferSize")
                            [Void]$ExemptionList.Add("DeleteMode")
                            [Void]$ExemptionList.Add("RecycleMode")
                            [Void]$ExemptionList.Add("ManifestMode")
                            [Void]$ExemptionList.Add("ExampleMode")
                            [Void]$ExemptionList.Add("ExampleList")
                            [Void]$ExemptionList.Add("ShowError")
                            [Void]$ExemptionList.Add("TrayIcon")

                            # Remove Variable
                            ForEach ($Variable in $VariableList) {
                                if (!($ExemptionList.Contains($Variable))) {
                                    Remove-Variable -Name $Variable -ErrorAction "SilentlyContinue"
                                }
                            }

                        }

                        "F2" {

                            # System Tray Icon Removal
                            $TrayIcon.Visible = $False

                            # Terminate Process
                            Stop-Process -ID $PID

                        }

                        "ENTER" {

                            # System Tray Message
                            $TrayIcon.Text = "Status Report" + "`n" + "• File Stage: None" + "`n" + "• Percent Done: None"

                            # Return to Main Menu
                            Remove-Variable -Name "RunningMode" -ErrorAction "SilentlyContinue"
                            Remove-Variable -Name "ProgramExecuted" -ErrorAction "SilentlyContinue"
                            $MarkedComplete = $True

                            # Master Variable Refresh
                            $PowerShell = [PowerShell]::Create()
                            $PowerShell.AddScript('Get-Variable | Select-Object -ExpandProperty "Name"') | Out-Null
                            $VariableList = $PowerShell.Invoke()
                            $PowerShell.Dispose()
                            $VariableList = Get-Variable | Select-Object -ExpandProperty "Name" | Where-Object { $VariableList -NotContains $_ }

                            # Define Ignore List
                            $ExemptionList = [System.Collections.Generic.List[Object]]::New()
                            [Void]$ExemptionList.Add("ErrorActionPreference")
                            [Void]$ExemptionList.Add("OutputEncoding")
                            [Void]$ExemptionList.Add("VariableList")
                            [Void]$ExemptionList.Add("ExemptionList")
                            [Void]$ExemptionList.Add("MarkedComplete")
                            [Void]$ExemptionList.Add("TargetPath")
                            [Void]$ExemptionList.Add("TargetDestination")
                            [Void]$ExemptionList.Add("BufferSize")
                            [Void]$ExemptionList.Add("DeleteMode")
                            [Void]$ExemptionList.Add("RecycleMode")
                            [Void]$ExemptionList.Add("ManifestMode")
                            [Void]$ExemptionList.Add("ExampleMode")
                            [Void]$ExemptionList.Add("ExampleList")
                            [Void]$ExemptionList.Add("ShowError")
                            [Void]$ExemptionList.Add("TrayIcon")

                            # Remove Variable
                            ForEach ($Variable in $VariableList) {
                                if (!($ExemptionList.Contains($Variable))) {
                                    Remove-Variable -Name $Variable -ErrorAction "SilentlyContinue"
                                }
                            }

                            # Remove Remaining Variable
                            Remove-Variable -Name "VariableList" -ErrorAction "SilentlyContinue"
                            Remove-Variable -Name "ExemptionList" -ErrorAction "SilentlyContinue"

                        }

                        "BACKSPACE" {

                            # System Tray Icon Removal
                            $TrayIcon.Visible = $False

                            # Terminate Process
                            Stop-Process -ID $PID

                        }

                        { $True } {

                            # Reset Variable
                            Remove-Variable -Name "ShownMessage" -ErrorAction "SilentlyContinue"

                        }

                    }

                }

                # Console Pause
                [Threading.Thread]::Sleep(500)

            }

            # Reset Variable
            Remove-Variable -Name "MarkedComplete" -ErrorAction "SilentlyContinue"

        }

    }

}